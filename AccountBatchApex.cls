public class AccountBatchApex implements Database.Batchable<sObject>,Database.AllowsCallouts{
    public List<Account> myList;
    String[] updateAttributes = new String[]{};
        Integer[] updateVals = new Integer[]{};
            public Map<Id,string> taskmaps = new Map<Id,string>() ;
    public Map<Id,CustomAttributeMapping> customAttr = new Map<Id,CustomAttributeMapping>() ;
    
    public AccountBatchApex(List<Account> passedList, String[] passedAttributes, Integer[] passedVals){
        myList = passedList;
        updateattributes = passedAttributes;   
        updateVals = passedVals;
    }
    public AccountBatchApex(List<Account> passedList,Map<Id,CustomAttributeMapping> passedCustAttMap){
        customAttr = passedCustAttMap;
        updateAttributes.add('custom attributes');
        myList = passedList;        
        
    }
    public List<Account> start(Database.BatchableContext bc){
        
        return myList;
    }
    public void execute(Database.BatchableContext bc, List<Account> scope){
        
        
        CustomAttributeMapping myEnvironment = new CustomAttributeMapping();
        for(Account recID:scope){
            
            system.debug(updateAttributes[0] + ' is the attribute code');
            if(updateAttributes[0] == 'custom attributes'){
                try{
                    JsonGen reqBody = new JsonGen();
                    CustomAttributeMapping attVals = customAttr.get(recID.id);
                    string reqStr = reqBody.jsonGeneratorPaymentMethodChange2(recID,attVals);
                    Http http = new Http();
                    HttpRequest request = new HttpRequest();
                    request.setEndpoint(myEnvironment.endpoint +recID.Beverage_Portal_ID__c); //pass in external id 
                    request.setMethod('PUT');
                    request.setHeader('Authorization', 'Bearer ' + myEnvironment.auth);
                    request.setHeader('Content-Type', 'application/json');
                    
                    request.setBody(reqStr);
                    HTTPResponse res = http.send(request);
                    System.debug(res.getBody());
                    taskmaps.put(recID.Id,reqStr );
                    
                    
                }
                catch(Exception e){
                    System.debug('The following exception has occurred: ' + e.getMessage());   
                }
            }
            
            
            if(updateAttributes[0] == 'address Changed'){
                
                
                try{
                    Address addr = recID.BillingAddress;
                    string street2 =recID.Billing_Address_2__c;                                  
                    JsonGen reqBody = new JsonGen();
                    string reqStr = reqBody.jsonGeneratorAddressChange(addr.getStreet(),street2 ,addr.getCity(),addr.getState(),
                                                                       addr.getPostalCode(), Integer.valueOf(recID.AddressID__c),recID.tz__Timezone_IANA__c  );   
                    Http http = new Http();
                    HttpRequest request = new HttpRequest();             
                    request.setEndpoint( myEnvironment.endpoint +recID.Beverage_Portal_ID__c); //pass in external id 
                    request.setMethod('PUT');
                    request.setHeader('Authorization', 'Bearer ' + myEnvironment.auth);
                    request.setHeader('Content-Type', 'application/json');
                    request.setBody(reqStr);
                    HTTPResponse res = http.send(request);
                    System.debug(res.getBody());
                    if(recID.tz__Timezone_IANA__c==null){
                    reqStr = 'Error: the timezone was not available for outbound messages at this time \n'+ reqStr;    
                    }
                    taskmaps.put(recID.Id,reqStr );
                    
                    
                    
                }
                catch(Exception e){
                    System.debug('The following exception has occurred: ' + e.getMessage());   
                }
            }
            
            else if (updateAttributes[0] == 'phone Changed') {
                HttpResponse res = JsonGen.PhoneChange(recID);
                try{
                    string reqStr = res.getBody();
                    taskmaps.put(recID.Id,reqStr );
                } 
                catch(Exception e){
                    System.debug('The following exception has occurred: ' + e.getMessage()); 
                    
                }  
            }
            else if (updateAttributes[0] == 'name Changed') {
                HttpResponse res = JsonGen.NameChange(recID);
                try{
                    string reqStr = res.getBody();
                    taskmaps.put(recID.Id,reqStr );
                } 
                catch(Exception e){
                    System.debug('The following exception has occurred: ' + e.getMessage()); 
                    
                }
            } 
            else if (updateAttributes[0] == 'name2 Changed') {
                HttpResponse res = JsonGen.Name2Change(recID);
                try{
                    string reqStr = res.getBody();
                    taskmaps.put(recID.Id,reqStr );
                } 
                catch(Exception e){
                    System.debug('The following exception has occurred: ' + e.getMessage()); 
                    
                }
            } 
            
            else if (updateAttributes[0] == 'Fully Activated') {
                HttpResponse res= JsonGen.FullyActivate(recID);
                try{
                    string reqStr = res.getBody();
                 // if(recID.tz__Timezone_IANA__c==null){
                 //   reqStr = 'Error: the timezone was not available for outbound messages at this time \n'+ reqStr;    
                 //   }
                    taskmaps.put(recID.Id,reqStr );
                } 
                catch(Exception e){
                    System.debug('The following exception has occurred: ' + e.getMessage()); 
                    
                }
            }
            
            
            
        }
        
        
        TaskLoggingQueueableAcc tasksync = new TaskLoggingQueueableAcc(taskmaps);
        System.enqueueJob(tasksync);   
        
        
    }
    public void finish(Database.BatchableContext bc){
        
    }
    
}